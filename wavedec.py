# Importing essential libraries
import pandas as pd
import numpy as np
import pywt
from pywt import wavedec

# Load the univariate signal
df=pd.read_csv('wind_speed_data.csv')
print(df.head())


# Let x be the univariate signal of interest
x=df.iloc[:,0]
n=len(x)
print(n)

# Perform 5-level  discrete wavelet transform
coeff=pywt.wavedec(x,'db4',level=5)
cA5, cD5, cD4, cD3, cD2, cD1,=coeff 



A5=pywt.upcoef('a',cA5,'db4',level=5,take=n)
D5=pywt.upcoef('d',cD5,'db4',level=5,take=n)
D4=pywt.upcoef('d',cD4,'db4',level=5,take=n)
D3=pywt.upcoef('d',cD3,'db4',level=5,take=n)
D2=pywt.upcoef('d',cD2,'db4',level=5,take=n)
D1=pywt.upcoef('d',cD1,'db4',level=5,take=n)

fea={'A5':A5,'D5':D5,'D4':D4,'D3':D3,'D2':D2,'D1':D1}
features=pd.DataFrame(fea)
print(features)

